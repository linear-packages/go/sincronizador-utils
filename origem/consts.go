package origem

// Constants que definem o tipo da Operação da mensagem de sincronização de dados
const (
	// Dominio Seguranca
	SegurancaCadastrosUsuario = "seguranca.cadastros.usuario"

	// Dominio Cadastros
	CadastrosEmpresa  = "cadastros.tabelas_auxiliares.empresa"
	CadastrosParceiro = "cadastros.tabelas_auxiliares.parceiro"
)
