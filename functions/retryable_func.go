package functions

import (
	"time"

	lgr "gitlab.com/linear-packages/go/logger"
)

var logger = lgr.New("main")

type RetryableFuncParameters struct {
	AttemptsCount int
	Function      func() error
	SuccessMsg    string
	ErroMsg       string
}

func CallRetryableFunc(c RetryableFuncParameters) error {
	var err error
	if c.AttemptsCount > 0 {
		err = retryLimited(c)
	} else {
		err = retryUnlimited(c)
	}
	return err
}

func retryLimited(c RetryableFuncParameters) error {
	var err error
	timeMultiplier := 1
	for attemptsCount := 1; attemptsCount <= c.AttemptsCount; attemptsCount++ {
		err = c.Function()
		if err == nil {
			logger.Info(c.SuccessMsg)
			return nil
		}
		if attemptsCount <= c.AttemptsCount {
			timeMultiplier *= 2
			waitTime := time.Duration(timeMultiplier*5) * time.Second
			logger.Error(c.ErroMsg)
			logger.Error("Tentativas ->", attemptsCount)
			if attemptsCount != c.AttemptsCount {
				time.Sleep(waitTime)
			}
		}
	}
	return err
}

func retryUnlimited(c RetryableFuncParameters) error {
	var err error
	for {
		err = c.Function()
		if err == nil {
			logger.Info(c.SuccessMsg)
			return nil
		}
		logger.Error(c.ErroMsg)
		time.Sleep(time.Second * 5)
	}
}
