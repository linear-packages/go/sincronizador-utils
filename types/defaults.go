package types

import "gitlab.com/linear-packages/go/sincronizador-utils/operacao"

// Executor é a interface que representa a operação dentro dos services, responsável
// pelo sincronismo de cada entidade.
type Executor interface {
	Processar(mensagem *Mensagem) error
}

type Mensagem struct {
	Origem   string                `json:"origem"`
	Operacao operacao.TipoOperacao `json:"operacao"`
	Payload  interface{}
}
