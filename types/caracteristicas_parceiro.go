package types

import "slices"

type CaracteristicasParceiro []int16

const (
	Colaborador int16 = 1
	Fornecedor  int16 = 2
	Cliente     int16 = 4
	Contato     int16 = 8
)

func (c CaracteristicasParceiro) EhCliente() bool {
	return slices.Contains(c, Cliente)
}

func (c CaracteristicasParceiro) EhFornecedor() bool {
	return slices.Contains(c, Fornecedor)
}

func (c CaracteristicasParceiro) EhColaborador() bool {
	return slices.Contains(c, Colaborador)
}

func (c CaracteristicasParceiro) EhContato() bool {
	return slices.Contains(c, Contato)
}
