package types

type StatusGeneric int16

func (s StatusGeneric) Active() StatusGeneric {
	return 0
}

func (s StatusGeneric) Inactive() StatusGeneric {
	return 1
}

func (s StatusGeneric) Deleted() StatusGeneric {
	return 2
}

func (s StatusGeneric) Ids() []interface{} {
	return []interface{}{s.Active(), s.Inactive(), s.Deleted()}
}
