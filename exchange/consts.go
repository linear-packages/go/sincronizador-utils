// Package com constants que representam nomes das Exchanges usadas como Broadcaster
package exchange

// Constants que definem os nomes das exchanges dos Broadcasters
const (
	// Dominio Financeiro
	FinanceiroCadastros = "financeiro.cadastros.broadcaster"

	// Dominio Produto
	ProdutoCadastros = "produto.cadastros.broadcaster"

	// Dominio Vendas
	VendasCadastros = "vendas.cadastros.broadcaster"

	// Dominio Fiscal
	FiscalCadastros = "fiscal.cadastros.broadcaster"

	// Cadastros
	CadastrosTabelasAuxiliares = "cadastros.tabelas_auxiliares.broadcaster"
)
