package operacao

type TipoOperacao string

// Constants que definem o tipo da Operação da mensagem de sincronização de dados
const (
	Inclusao       = "inclusao"
	Atualizacao    = "atualizacao"
	ExclusaoLogica = "exclusao-logica"
	ExclusaoFisica = "exclusao-fisica"
	Reenfileirar   = "reenfileirar"
)

func (t TipoOperacao) EhInclusao() bool {
	return t == Inclusao
}

func (t TipoOperacao) EhAtualizacao() bool {
	return t == Atualizacao
}
